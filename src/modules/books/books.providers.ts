import {Connection} from 'mongoose';
import {getModelToken} from '@nestjs/mongoose';
import {BookSchema} from '../../schema/books.schema';

export const booksProviders = [
    {
        provide: getModelToken('Book'),
        useFactory: (connection: Connection) => connection.model('Book', BookSchema),
        inject: ['DbConnectionToken']
    }
];
