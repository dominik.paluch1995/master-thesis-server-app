import {Module} from '@nestjs/common';
import {MongooseModule} from '@nestjs/mongoose';
import {DatabaseModule} from '../database/database.module';
import {BookSchema} from '../../schema/books.schema';
import {BooksService} from './books.service';
import {booksProviders} from './books.providers';
import {BooksController} from './books.controller';

@Module({
    imports: [
        MongooseModule.forFeature([{name: 'Book', schema: BookSchema}]),
        DatabaseModule
    ],
    providers: [BooksService, ...booksProviders],
    controllers: [BooksController]
})
export class BooksModule {

}
