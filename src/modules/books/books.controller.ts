import {Body, Controller, Delete, Get, Param, Post, Put} from '@nestjs/common';
import {BooksService} from './books.service';
import {Book} from '../../models/book';
import {CreateBookDto} from '../../dto/create-book.dto';

@Controller('books')
export class BooksController {
    constructor(private readonly booksService: BooksService) {

    }

    @Get()
    async findAll(): Promise<Book[]> {
        return this.booksService.findAll();
    }

    @Post()
    async create(@Body()createBookDto: CreateBookDto) {
        return this.booksService.create(createBookDto);
    }

    @Delete(':id')
    async remove(@Param('id') id) {
        return this.booksService.remove(id);
    }

    @Put(':id')
    async update(@Param('id') id, @Body() updatedBook) {
        return this.booksService.update(id, updatedBook);
    }
}
