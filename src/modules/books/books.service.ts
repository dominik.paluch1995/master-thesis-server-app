import {Injectable} from '@nestjs/common';
import {Book} from '../../models/book';
import {Model} from 'mongoose';
import {InjectModel} from '@nestjs/mongoose';
import {CreateBookDto} from '../../dto/create-book.dto';

@Injectable()
export class BooksService {
    constructor(@InjectModel('Book') private readonly bookModel: Model<Book>) {
    }

    async findAll(): Promise<Book[]> {
        return await this.bookModel.find().exec();
    }

    async create(createBookDto: CreateBookDto): Promise<Book> {
        const createdBook = new this.bookModel(createBookDto);
        return await createdBook.save();
    }

    async update(id: string, updatedBookDto: CreateBookDto): Promise<Book> {
        return await this.bookModel.findByIdAndUpdate(id, updatedBookDto, {new: true}).exec();
    }

    async remove(id: string) {
        return await this.bookModel.findByIdAndRemove(id).exec();
    }
}
