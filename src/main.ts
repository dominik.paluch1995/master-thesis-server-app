import {NestFactory} from '@nestjs/core';
import {AppModule} from './app.module';

async function bootstrap() {
    const app = await NestFactory.create(AppModule);
    await app.listen(3000);
}

bootstrap();


// import { NestFactory } from '@nestjs/core';
// import { AppModule } from './app.module';
// import * as express from 'express';
//
// async function bootstrap() {
//     const server = express();
//     // server.post('/books');
//
//
//     const app = await NestFactory.create(AppModule, server);
//     await app.listen(3000);
// }
// bootstrap();
