import {Module} from '@nestjs/common';
import {BooksModule} from './modules/books/books.module';
import {MongooseModule} from '@nestjs/mongoose';

@Module({
    imports: [BooksModule, MongooseModule.forRoot('mongodb://localhost:27017/nest')],
    controllers: [],
    providers: [],
})
export class AppModule {
}

MongooseModule.forRootAsync({
    useFactory: () => ({
        uri: 'mongodb://localhost:27017/nest',
    }),
});
