export interface Author {
    readonly _id: string;
    readonly name: string;
    readonly surname: string;
    readonly nationality: string;
}
