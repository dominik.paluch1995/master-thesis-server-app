import {Author} from './author';
import {CategoryType} from './category-type';

export interface Book {
    readonly _id: string;
    readonly title: string;
    readonly author: Author;
    readonly releaseDate: Date;
    readonly publishingHouse: string;
    readonly isbn: string;
    readonly category: CategoryType
}
