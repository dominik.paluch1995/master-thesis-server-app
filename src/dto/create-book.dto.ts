import {IsDate, IsEnum, IsISBN, IsString} from 'class-validator';
import {Author} from '../models/author';
import {CategoryType} from '../models/category-type';

export class CreateBookDto {
    @IsString() readonly title: string;
    readonly author: Author;
    @IsDate() releaseDate: Date;
    @IsString() publishingHouse: string;
    @IsISBN() isbn: string;
    @IsEnum(["COMEDY", "DRAMA", "SCIENCE_FICTION", "ROMANCE"]) category: CategoryType;

}
