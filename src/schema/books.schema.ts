import * as mongoose from 'mongoose';

export const BookSchema = new mongoose.Schema({
    title: String,
    author: {
        name: String,
        surname: String,
        nationality: String
    },
    releaseDate: Date,
    publishingHouse: String,
    isbn: String,
    category: String
});
